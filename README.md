# README #

Aragon: Autonomous Remote Controlled Ground Obstacles Avoider Navigator

### What is this repository for? ###

Contains source code implementation of Autonomous Car using Neural Networks on iRobot Create

### Contributors ###
Himanshu Arora - hiarora@ucsd.edu 
Jayant Malani - jmalani@ucsd.edu
Siddhant Arya - sarya@ucsd.edu